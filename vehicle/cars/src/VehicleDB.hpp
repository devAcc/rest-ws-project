//
// Created by eguolyi on 2018-05-27.
//

#ifndef VEHICLE_VEHICLEDB_HPP
#define VEHICLE_VEHICLEDB_HPP
#include <string>
#include <cstdarg>
#include <sstream>
#include <vector>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>

//! The min data structure for interaction between ngrest server and mongoDB .
/**
* The structure Car is the main data structure which is used to form database collection.
* Also it is the content of a json document
*/
struct Car {
    std::string vin;
    std::string maker;
    std::string model;
    std::string owner;
    std::string color;
    std::string city;
    int modelyear;


    std::string toString() const {
        std::ostringstream buf;
        buf << "<br>Car{vin=" << vin
            << ", maker=" << maker
            << ", model=" << model
            << ", owner=" << owner
            << ", color=" << color
            << ", city=" << city
            << ", modelyear=" << modelyear
            << "}" ;
        return buf.str();
    }

};

//! The interface to talk to MongoDB database
/**
* Class VehicleDB is used to interact with car database: read, insert, update car...
*/
class VehicleDB {
    mongocxx::instance dbDriver{};
    mongocxx::client dbClient{mongocxx::uri{}};
public:
    static VehicleDB dbInstance;

    std::vector<Car> find(bool bAll, ...);
    Car documentToStructure(bsoncxx::document::view& v) const ;
    bool insertOne(Car& car);
    bool remove(std::vector<Car> car);
    bool update(const Car& car);
    int googleTest(int i) {return i;}
};


#endif //VEHICLE_VEHICLEDB_HPP
