//
// Created by eguolyi on 2018-05-27.
//

#include "VehicleDB.hpp"
#include <iostream>
#include <stdexcept>
#include <mongocxx/exception/bulk_write_exception.hpp>
using namespace std;
using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;
using namespace bsoncxx::document;
using namespace bsoncxx::builder::stream;


VehicleDB VehicleDB::dbInstance{};

/**
* Transform a bsoncxx Car document into C++ Car structure
* @param[in] bsoncxx::document::view v : the document
* @return structure Car
* @see
*/
Car VehicleDB::documentToStructure(view& v) const {
    auto vin = v["vin"].get_utf8().value.to_string();
    auto maker = v["maker"].get_utf8().value.to_string();
    auto model = v["model"].get_utf8().value.to_string();
    auto owner = v["owner"].get_utf8().value.to_string();
    auto color = v["color"].get_utf8().value.to_string();
    auto city = v["city"].get_utf8().value.to_string();
    auto modelyear = v["modelYear"].get_int32().value;

    return {vin,maker,model,owner,color,city,modelyear};
}

/**
* Find cars based on Variadic arguments which forms the match filter
* @param[in] bool bAll : true to retrieve multiple document, false to retrieve one document
* @return vector<Car>
* @see
*/
vector<Car> VehicleDB:: find(bool bAll, ...) {
    // use either the database() method or operator[] to obtain a mongocxx::database instance
    mongocxx::database db = dbClient["vehicle"];
    //  use either the collection() method or operator[] to obtain a mongocxx::collection instance.
    mongocxx::collection coll = db["car"];
    vector<Car> result;
    if (bAll) {
        auto cur = coll.find({});
        for (auto doc : cur) {
            result.push_back(documentToStructure(doc));
        }

    } else {
        va_list args;
        va_start(args,bAll);
        string name= va_arg(args, string);
        string value= va_arg(args, string);
        va_end(args);
        if( name == "modelYear") {
            auto cur = coll.find(make_document(kvp("modelYear",stoi(value.c_str()))));
            for (auto doc : cur) {
                result.push_back(documentToStructure(doc));
            }
        }  else {
            auto cur = coll.find(make_document(kvp(name,value)));
            for (auto doc : cur) {
                result.push_back(documentToStructure(doc));
            }
        }
    }

    return result;
}

/**
* insert one car record
* @param
* @return
* @see
*/
bool VehicleDB::insertOne(Car &car) {
    mongocxx::database db = dbClient["vehicle"];
    mongocxx::collection coll = db["car"];
    try {
        auto build = document{} << "vin" << car.vin << "maker" << car.maker << "model" << car.model
                                   << "owner" << car.owner << "color" << car.color << "modelYear" << car.modelyear << "city" << car.city << finalize;
        coll.insert_one(build.view()) ;

    } catch (const mongocxx::bulk_write_exception& e){
        cout << "[VehicleDB::insertOne: failed to insert document!" << endl
             << e.what() << endl;
    }


    return false;
}

/**
* remove one car record
* @param
* @return
* @see
*/
bool VehicleDB::remove(vector<Car> car) {

    mongocxx::database db = dbClient["vehicle"];
    mongocxx::collection coll = db["car"];
    try {
        for(auto& c : car) {
            coll.delete_one(make_document(kvp ("vin", c.vin)));
        }
    } catch (const mongocxx::bulk_write_exception& e){
        cout << "[VehicleDB::insertOne: failed to insert document!" << endl
             << e.what() << endl;
        return false;
    }

    return true;
}

/**
* update one car record
* @param
* @return
* @see
*/
bool VehicleDB::update(const Car &car) {
    mongocxx::database db = dbClient["vehicle"];
    mongocxx::collection coll = db["car"];
    try {

            coll.update_one(make_document(kvp ("vin", car.vin)), document{} << "$set" << open_document << "maker" << car.maker << "model" << car.model
            << "owner" << car.owner << "color" << car.color << "modelYear" << car.modelyear << "city" << car.city << close_document << finalize);

    } catch (const mongocxx::bulk_write_exception& e){
        cout << "[VehicleDB::update: failed to update document!" << endl
             << e.what() << endl;
        return false;
    }

    return true;
}
