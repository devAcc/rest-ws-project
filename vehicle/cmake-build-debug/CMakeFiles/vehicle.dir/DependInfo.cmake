# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/eguolyi/Development/WebService/rest-ws-project/vehicle/ServiceTest.cpp" "/home/eguolyi/Development/WebService/rest-ws-project/vehicle/cmake-build-debug/CMakeFiles/vehicle.dir/ServiceTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "deploy/include"
  "/home/eguolyi/.ngrest/ngrest-build/deploy/include"
  "/home/eguolyi/lib/googletest-release-1.8.0/googletest/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
