Short description
====
To practise C++ coding, I designed and implemented a small REST-WS based client-service micro-services system, using a simple C++ REST framework(Ngrest) and NoSQL backend(MongoDB).

What are included
====
* An authenticated REST-WS API C++ server that delivers JSON product data retrieved from a MongoDB server.
* Support all CRUD (Create Read Update Delete) operations, after a successful JWT authentication.
* A simple REST-WS client that performs a CRUD suite of calls to the server
* Unit tests were done in Google Test/Mock
* All function tests for the server are done by using shell-scripts and HTTPie.
* All source codes are documented using doxygen

More Readings
====
* [MongoDB Details](./mongodb.md)
* [REST-WS Details](./rest-ws.md)

More Information
====
To implement authentication, a 3rd-party C++14 library for JWT was used (https://github.com/arun11299/cpp-jwt)
.

