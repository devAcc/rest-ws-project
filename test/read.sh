#!/usr/bin/env bash
set -ue

BASE_URI=http://localhost:9098/cars/all

echo "--- ALL ---"
http -v get ${BASE_URI}/
