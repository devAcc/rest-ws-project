#!/usr/bin/env bash
set -ue

BASE_URI=http://localhost:9098/books

echo "--- ALL ---"
http -v ${BASE_URI}

echo "--- CREATE ---"
BOOK='{"book":{"isbn":"4242", "author":"Per Silja", "title":"REST WS Cook Book", "pages":250}}'
http -v post ${BASE_URI} <<<${BOOK}

echo "--- ALL ---"
http -v ${BASE_URI}

echo "--- READ ---"
http -v get ${BASE_URI}/4242

echo "--- UPDATE ---"
BOOK='{"book":{"isbn":"4242", "author":"Anna Gram", "title":"REST Web Services Primer", "pages":300}}'
http -v put ${BASE_URI}/4242 <<<${BOOK}

echo "--- READ ---"
http -v get ${BASE_URI}/4242

echo "--- DELETE ---"
http -v delete ${BASE_URI}/4242

echo "--- READ ---"
http -v get ${BASE_URI}/4242 || echo OK

echo "--- ALL ---"
http -v ${BASE_URI}


https://jwt.io/introduction/
https://stackoverflow.com/questions/33265812/best-http-authorization-header-type-for-jwt
https://github.com/teracyhq/httpie-jwt-auth

Sending authenticaton:
http -v GET http://localhost:9098/cars User-Agent:httpie x-auth-token:xxx-my-token



_____________________________________________

#!/usr/bin/env bash
set -ue

BASE_URI=http://localhost:9098/products

echo "--- ALL ---"
#!http -v ${BASE_URI}

##echo "--- CREATE ---"
http --verbose POST localhost:9098/products/  product:='{"product_id":36,"name":"Zamit","version":"7.3.6","price":"$6.67"}' -a 2:abcd
#!PRODUCT="product:='{"product_id":"31","name":"application4", "version":"1.2","price":"20000"}''
#!http --verbose POST ${BASE_URI}/ $PRODUCT

##echo "--- READ ---"
http --verbose GET localhost:9098/products/one?id=36 -a 2:abcd

echo "--- UPDATE ---"
http --verbose PUT localhost:9098/products/  product:='{"product_id":36,"name":"Zamit","version":"7.3.6","price":"$9.08"}' -a 2:abcd
#!PRODUCT='product:='{"product_id":"31","name":"application31", "version":"1.1","price":"10000"}''
#!http --verbose UPDATE ${BASE_URI}/ ${PRODUCT}

echo "--- READ ---"
http --verbose GET localhost:9098/products/one?id=36 -a 2:abcd

echo "--- DELETE ---"
#!http --verbose DELETE localhost:9098/products/3
http --verbose DELETE localhost:9098/products/36 -a 2:abcd

echo "--- READ ---"
http --verbose GET localhost:9098/products/one?id=36 -a 2:abcd || echo ok

#!echo "--- ALL ---"
#!http -v ${BASE_URI}


#http --verbose POST localhost:9098/products/  product:='{"product_id":36,"name":"Zamit","version":"7.3.6","price":"$6.67"}' -a 2:abcd
#http --verbose GET localhost:9098/products/one?id=36 -a 2:abcd
#http --verbose PUT localhost:9098/products/  product:='{"product_id":36,"name":"Zamit","version":"7.3.6","price":"$9.08"}' -a 2:abcd
#http --verbose GET localhost:9098/products/one?id=36 -a 2:abcd
#http --verbose DELETE localhost:9098/products/36 -a 2:abcd
#http --verbose GET localhost:9098/products/one?id=36 -a 2:abcd || echo ok



