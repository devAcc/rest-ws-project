//
// Created by eguolyi on 2018-05-31.
//

#include "gtest/gtest.h"

#include "../vehicle/cars/src/VehicleDB.hpp"

using testing::TestWithParam;
using testing::Values;
using testing::Range;
using testing::KilledBySignal;
using testing::UnitTest;

TEST(GoogleTestExample, regular) {
    EXPECT_EQ(VehicleDB::dbInstance.googleTest(5), 5);
}

TEST(RemoveTest, regular) {

    Car car = {"G4GE5G3XCF613902", "Lexus", "IS", "Lynn Gerriets", "Khaki", "Kawangu", 1996 } ;
    std::string result = VehicleDB::dbInstance.find(false, std::string("vin"), std::string("G4GE5G3XCF613902")).front().toString();
    std::string expect = car.toString();
    EXPECT_EQ(result, expect);
}



