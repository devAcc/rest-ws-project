#!/bin/bash

GTEST=/home/eguolyi/lib/googletest-release-1.8.0/

g++-8 -Wall -Wextra -Wfatal-errors -std=c++17 mongoTest.cpp ../vehicle/cars/src/VehicleDB.cpp -I${GTEST}/googletest/include -I/usr/local/include/mongocxx/v_noabi -I/usr/local/include/bsoncxx/v_noabi -L/usr/local/lib -L${GTEST}/lib  -lgtest -lgtest_main -lmongocxx -lbsoncxx -pthread -o mongoTest
